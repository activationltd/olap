select NON EMPTY {[Measures].[Sanctioned Posts], [Measures].[Female], [Measures].[Male], [Measures].[Vacant], [Measures].[Filled]} ON COLUMNS,
  NON EMPTY {[Area].[All Area]} ON ROWS
from [SanctionedPostCube]

select NON EMPTY {[Measures].[Providers]} ON COLUMNS,
  NON EMPTY {[Age].[All Age]} ON ROWS
from [ProviderCube]

select NON EMPTY {[Measures].[Providers]} ON COLUMNS,
  NON EMPTY Hierarchize({([Department].[All Department], [Maritalstatus].[All Maritalstatus], [Sex].[All Sex], [Tribaloption].[All Tribaloption])}) ON ROWS
from [ProviderCube]