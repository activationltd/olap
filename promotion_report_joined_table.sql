
create table reporting_acr_mark_year_average_provider_temp
SELECT 
DISTINCT provider_id, 
GROUP_CONCAT(DISTINCT `year` ORDER BY year SEPARATOR '\n') AS acr_year,
GROUP_CONCAT(DISTINCT `year`, ' : ' ,acrmark1 ORDER BY year SEPARATOR '\n') AS marks
FROM dghshrml4_acrs
group by provider_id;
ALTER TABLE `reporting_acr_mark_year_average_provider_temp` ADD INDEX(`provider_id`);
drop table if exists reporting_acr_mark_year_average_provider;
rename table reporting_acr_mark_year_average_provider_temp to reporting_acr_mark_year_average_provider;
create table reporting_acr_mark_year_with_pr_temp

SELECT 
provider_id,
year,
CASE
when acrmark1 LIKE '1%' THEN acrmark1
when acrmark1 LIKE '2%' THEN acrmark1
when acrmark1 LIKE '3%' THEN acrmark1
when acrmark1 LIKE '4%' THEN acrmark1
when acrmark1 LIKE '5%' THEN acrmark1
when acrmark1 LIKE '6%' THEN acrmark1
when acrmark1 LIKE '7%' THEN acrmark1
when acrmark1 LIKE '8%' THEN acrmark1
when acrmark1 LIKE '9%' THEN acrmark1
when acrmark1 LIKE '0%' THEN acrmark1
ELSE 'PR'
END
AS acrmarks 
FROM dghshrml4_acrs;
DELETE FROM reporting_acr_mark_year_with_pr_temp
WHERE acrmarks  LIKE '%PR%';
drop table if exists reporting_acr_mark_year_without_pr;
rename table reporting_acr_mark_year_with_pr_temp to reporting_acr_mark_year_without_pr;
ALTER TABLE `reporting_acr_mark_year_without_pr` ADD INDEX(`provider_id`);


create table reporting_acr_mark_year_average_without_pr_temp
SELECT 
DISTINCT provider_id, 
ROUND(AVG(acrmarks),2) AS average
FROM reporting_acr_mark_year_without_pr
group by provider_id;
ALTER TABLE `reporting_acr_mark_year_average_without_pr_temp` ADD INDEX(`provider_id`);
drop table if exists reporting_acr_mark_year_average_without_pr;
rename table reporting_acr_mark_year_average_without_pr_temp to reporting_acr_mark_year_average_without_pr;


create table reporting_promotion_first_table_temp
SELECT
	dghshrml4_designations.`name` as designations_name,
	dghshrml4_designations.id as designations_id,
	dghshrml4_designationgroups.id AS designationgroups_id,
	dghshrml4_designationdisciplines.id AS professionaldisciplines_id,
	dghshrml4_designationgroups.name AS designationgroups_name,
	dghshrml4_designationdisciplines.name AS professionaldisciplines_name,
	dghshrml4_providers.desired_designation_group as providers_desired_designation_group,
	dghshrml4_providers.desired_professional_discipline as providers_desired_professional_discipline

FROM
	dghshrml4_sanctionedposts
LEFT JOIN dghshrml4_designations ON dghshrml4_sanctionedposts.designation_id = dghshrml4_designations.id
LEFT JOIN dghshrml4_facilities ON dghshrml4_sanctionedposts.facility_id = dghshrml4_facilities.id
LEFT JOIN dghshrml4_providers ON dghshrml4_providers.sanctionedpost_id = dghshrml4_sanctionedposts.id
LEFT JOIN dghshrml4_designationgroups ON dghshrml4_providers.desired_designation_group = dghshrml4_designationgroups.id
LEFT JOIN dghshrml4_designationdisciplines ON dghshrml4_providers.desired_professional_discipline = dghshrml4_designationdisciplines.id
WHERE
dghshrml4_sanctionedposts.deleted_at IS NULL
AND dghshrml4_sanctionedposts.is_active = 1
AND dghshrml4_providers.sanctionedpost_id > 0
GROUP BY
	professionaldisciplines_name,
	designationgroups_name
ORDER BY
	professionaldisciplines_name;
ALTER TABLE reporting_promotion_first_table_temp ENGINE=MyISAM;
ALTER TABLE `reporting_promotion_first_table_temp` ADD INDEX(`designations_id`);
ALTER TABLE `reporting_promotion_first_table_temp` ADD INDEX(`designationgroups_id`);
ALTER TABLE `reporting_promotion_first_table_temp` ADD INDEX(`professionaldisciplines_id`);
drop table if exists reporting_promotion_first_table;
rename table reporting_promotion_first_table_temp to reporting_promotion_first_table;

create table reporting_promotion_second_table_temp
SELECT
	dghshrml4_providers. NAME AS provider,
	dghshrml4_providers. id AS providers_id,
	dghshrml4_providers.providerstatus_id AS provider_status_id,
	dghshrml4_providers.providerstatus_name AS provider_status,
	DATE_FORMAT(
		dghshrml4_providers.`dob`,
		'%Y-%m-%d'
	) AS dob,
	designation_sanctioned.`designationdiscipline_name`,
	designation_sanctioned.`name` AS designation,
	designation_sanctioned.id,
	dghshrml4_providers.pds_code,
	DATE_FORMAT(
		dghshrml4_providers.joining_date_govt_health_service,
		'%Y-%m-%d'
	) AS joining_date_govt_health_service,
	dghshrml4_providers.joining_date_current_designation,
	dghshrml4_providers.service_confirmation_sl_no,
	DATE_FORMAT(
		dghshrml4_providers.service_confirmation_date,
		'%Y-%m-%d'
	) AS service_confirmation_date,

	DATE_FORMAT(
	dghshrml4_providers.joining_date_primary_designation,
	'%Y-%m-%d'
	) AS joining_date_primary_designation,
	
	dghshrml4_providers.primary_designation_name,
	dghshrml4_providers.primary_designation,
	YEAR (dghshrml4_providers.ist_date) as ist_date,
	dghshrml4_providers.ist_sl_no as ist_sl,
	dghshrml4_providers.bcs_psc_regularization_sl_no,
DATE_FORMAT(
		dghshrml4_providers.bcs_psc_regularization_date,
		'%Y-%m-%d'
	) AS bcs_psc_regularization_date,
	dghshrml4_providers.current_payscale,
	dghshrml4_providers.bcs_batch_no,
	dghshrml4_providers.providereduquals_cons,
	dghshrml4_providers.contact_no,
	dghshrml4_providers.desired_designation_group as prov_desired_des_group,
	dghshrml4_providers.desired_professional_discipline as prov_desired_prof_discipline,
	dghshrml4_providers.publication_national,
	dghshrml4_providers.publication_international,
	dghshrml4_providers.last_promotion_confirmed_designation,
	CASE
when dghshrml4_providers.is_senior_scale_pass=0 THEN 'No'
when dghshrml4_providers.is_senior_scale_pass=1 THEN 'Yes'
when dghshrml4_providers.is_senior_scale_pass=2 THEN 'Exempted'
END
as senior_scale,
	designation_last_promotion.`name` AS last_promotion_name,
	dghshrml4_providers.last_promotion_go,
	dghshrml4_providers.last_promotion_serial,
DATE_FORMAT(
	dghshrml4_providers.last_promotion_date,
	'%Y-%m-%d'
	) AS last_promotion_date,
	sanctioned_facility. `name` AS sanctioned_facility,
	additional_facility. `name` AS additional_facility,
	deputed_facility. `name` AS deputed_facility,
	reporting_acr_mark_year_average_without_pr.average AS average_acr,
	reporting_acr_mark_year_average_provider.marks AS acr_year_mark,
	reporting_acr_mark_year_average_provider.acr_year

FROM
	dghshrml4_sanctionedposts
LEFT JOIN dghshrml4_designations AS designation_sanctioned ON  dghshrml4_sanctionedposts.designation_id = designation_sanctioned.id 
LEFT JOIN dghshrml4_providers ON dghshrml4_providers.sanctionedpost_id = dghshrml4_sanctionedposts.id
LEFT JOIN dghshrml4_additionalroles ON dghshrml4_providers.id=dghshrml4_additionalroles.provider_id
LEFT JOIN dghshrml4_deputations ON dghshrml4_providers.id=dghshrml4_deputations.provider_id
LEFT JOIN dghshrml4_designations AS designation_last_promotion ON dghshrml4_providers.last_promotion_confirmed_designation = designation_last_promotion.id
LEFT JOIN dghshrml4_facilities AS sanctioned_facility ON dghshrml4_sanctionedposts.facility_id = sanctioned_facility.id
LEFT JOIN dghshrml4_facilities AS additional_facility ON dghshrml4_additionalroles.facility_id = additional_facility.id
LEFT JOIN dghshrml4_facilities AS deputed_facility ON dghshrml4_deputations.facility_id = deputed_facility.id
LEFT JOIN dghshrml4_designationgroups ON dghshrml4_providers.desired_designation_group = dghshrml4_designationgroups.id
LEFT JOIN dghshrml4_designationdisciplines ON dghshrml4_providers.desired_professional_discipline = dghshrml4_designationdisciplines.id
LEFT JOIN reporting_acr_mark_year_average_provider ON dghshrml4_providers.id = reporting_acr_mark_year_average_provider.provider_id
LEFT JOIN reporting_acr_mark_year_average_without_pr ON dghshrml4_providers.id = reporting_acr_mark_year_average_without_pr.provider_id

WHERE
dghshrml4_sanctionedposts.deleted_at IS NULL
AND dghshrml4_providers.providerstatus_id NOT IN (1,11,12,13,14,15)
AND dghshrml4_providers.deleted_at IS NULL
AND sanctioned_facility.deleted_at IS NULL
AND additional_facility.deleted_at IS NULL
AND deputed_facility.deleted_at IS NULL
AND dghshrml4_additionalroles.deleted_at IS NULL
AND dghshrml4_deputations.deleted_at IS NULL
AND designation_last_promotion.deleted_at IS NULL
AND dghshrml4_designationdisciplines.deleted_at IS NULL
AND designation_sanctioned.deleted_at IS NULL
GROUP BY
dghshrml4_providers.id;

ALTER TABLE reporting_promotion_second_table_temp ENGINE = MyISAM;
ALTER TABLE `reporting_promotion_second_table_temp` ADD INDEX( `providers_id`);
ALTER TABLE `reporting_promotion_second_table_temp` ADD INDEX( `prov_desired_des_group`);
ALTER TABLE `reporting_promotion_second_table_temp` ADD INDEX( `contact_no`);
ALTER TABLE `reporting_promotion_second_table_temp` ADD INDEX( `prov_desired_prof_discipline`);
drop table if exists reporting_promotion_second_table;
rename table reporting_promotion_second_table_temp to reporting_promotion_second_table;
